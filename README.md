# ePolisowanie - praca inżynierska

Do prawidłowego uruchomienia projektu potrzebny jest NodeJs (zalecana ostatnia wersja LTS).

1. Przed uruchomieniem należy w katalogu głównym uruchomić polecenie `npm install` w celu pobrania wszystkich zależności
2. Uruchomieć polecenie `ng build`, by zbudować projekt
3. Uruchomić polecenie `npm start`
4. Frontend aplikacji powinien być dostępny pod adresem `http://localhost:8080`
