import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import { Room } from '../models/Room';
import { HttpClient } from '@angular/common/http';
import {Period} from '../models/Period';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(private http: HttpClient) { }

  addRoom(room: Room): Observable<Room> {
    return this.http.post<Room>('/api/room/create', room);
  }

  getUserRooms(id: number): Observable<Room[]> {
    return this.http.get<Room[]>('/api/room/getAllByUser' +  id);
  }

  getRooms(): Observable<Room[]> {
    return this.http.get<Room[]>('/api/rooms');
  }

  // TODO podmienic na serwis
  getRoomsByDate(date: string): Observable<Room[]> {
    const room: Room = {id: 5, desks: [], numberOfDesks: 2, name: 'asd', image: 'test'};
    return this.getRooms();
  }

  saveFile(id: number, file: File): Observable<void> {
    const formData: FormData = new FormData();

    formData.append('multipartImage', file);
    formData.append('id', '' + id);
    return this.http.post<void>('/api/room/image/upload', formData);
  }

  reserveRoom(date: string, roomId: number, user: string, lp: number): Observable<Period> {
    return this.http.get<Period>('/api/room/reserve/' + date + '/' + roomId + '/' + user + '/' + lp);
  }

}
