import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { Md5 } from 'ts-md5';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private userService: UserService, private router: Router) { }

  authenticate(username, password): Observable <User> {
    password = Md5.hashStr(password);
    const subject = new Subject<User>();
    this.userService.correctUser({username, password})
      .subscribe( user => {
        if (user) {
          this.authenticateUser(user, subject);
        } else {
          subject.next(null);
        }
      });

    return subject.asObservable();
  }

  authenticateUser(user: User, subject: Subject<User>): void {
    sessionStorage.setItem('username', user.username);
    sessionStorage.setItem('role', user.role);
    subject.next(user);
    if (user.role === 'user') {
      this.router.navigate(['']);
    } else {
      this.router.navigateByUrl('edit-room');
    }
  }

  isUserLoggedIn(): boolean {
    const user = sessionStorage.getItem('role');
    return 'user' === user;
  }

  isAdminLoggedIn(): boolean {
    const user = sessionStorage.getItem('role');
    return 'admin' === user;
    //return true;
  }

  logOut(): void {
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('role');
  }

}
