import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {User} from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>('/api/users');
  }

  getUser(id): Observable<User> {
    return this.http.get<User>('/api/user/' + id);
  }

  correctUser(user: User): Observable<User> {
    if (user.username === 'user') {
      const user3: User = { username: 'test', password: 'asd', role: 'user'};
      return of(user3);
    }
    const user2: User = { username: 'test', password: 'asd', role: 'admin'};

    return of(user2);
  }

  addUser(user: User): Observable<User> {
    return this.http.put<User>('/api/add-user', user);
  }

  delete(id: number): Observable<User[]> {
    return this.http.delete<User[]>('/api/user/' + id);
  }

  editUser(user: User): Observable<User> {
    return this.http.put<User>('/api/editUser', user);
  }
}
