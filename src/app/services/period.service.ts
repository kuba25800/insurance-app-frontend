import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Period} from '../models/Period';
import {User} from '../models/User';
import {Room} from '../models/Room';
import {HttpClient} from '@angular/common/http';
import {UserDesk} from '../models/UserDesk';

@Injectable({
  providedIn: 'root'
})
export class PeriodService {

  constructor(private http: HttpClient) { }

  // TODO zamienici na serive
  getPeriodsByDateAndRoom(date: string, id: number): Observable<UserDesk[]> {
      return this.http.get<UserDesk[]>('/api/desk/findFreeByRoomIdAndDate/' + id + '/' + date);
  }

}
