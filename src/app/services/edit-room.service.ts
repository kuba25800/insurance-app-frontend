import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Room} from '../models/Room';

@Injectable({
  providedIn: 'root'
})
export class EditRoomService {

  constructor(private http: HttpClient) { }

  getRooms(): Observable<Room[]> {
    // return this.http.get<Room[]>('/api/rooms/');
    return of([{
      id: 1,
      name: 'Test',
      image: 'asd',
      numberOfDesks: 1,
      desks: []
    }, {
      id: 2,
      name: 'Tesfdsfdst',
      image: 'asd',
      numberOfDesks: 2,
      desks: []
    }]);
  }

}
