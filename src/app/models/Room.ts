import {Desk} from './Desk';

export interface Room {
  id?: number;
  name: string;
  image?: string;
  numberOfDesks: number;
  desks: Desk[];
}
