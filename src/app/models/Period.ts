import {User} from './User';

export interface Period {
  id?: number;
  lp: number;
  startDate: string;
  endDate: string;
  user: User;
}
