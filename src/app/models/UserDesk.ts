import {Desk} from './Desk';

export interface UserDesk {

  desk: Desk;
  username?: string;

}
