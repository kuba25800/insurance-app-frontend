import {Period} from './Period';

export interface Desk {
  id?: number;
  description: string;
  periods?: Period[];
  lp: number;
}
