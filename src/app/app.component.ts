import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'insurance-app-frontend';
  config: { [key: string]: string | Date } = null;

  constructor(public loginService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.config = {
      footer: 'Propertasy - rezerwacja miejsc',
      date: new Date()
    };
  }
}
