import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {Room} from '../../models/Room';
import {RoomService} from '../../services/room.service';

export interface DialogData {
  name: string;
  numberOfDesks: number;
  fileToUpload: File;
}

@Component({
  selector: 'app-add-user',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.css']
})
export class AddRoomComponent implements OnInit {

  model: Partial<Room> = {};
  fileToUpload: any;

  constructor(private userService: UserService,  public dialog: MatDialog, private roomService: RoomService) { }

  ngOnInit(): void {
  }

  addRoom(description: string, numberOfDesks: number): void {
    const dialogRef = this.dialog.open(AddUserDialogComponent, {
      width: '400px',
      data: {name: this.model.name, numberOfDesks: this.model.numberOfDesks, fileToUpload: this.fileToUpload}
    });
  }

  handleFileInput(files: FileList): void {
    this.fileToUpload = files.item(0);
  }

  addImage(): void {
    this.roomService.saveFile(10, this.fileToUpload).subscribe();
  }

  fileSelected(): boolean {
    return this.fileToUpload != null;
  }
}

@Component({
  selector: 'app-add-user-dialog-component',
  templateUrl: 'add-user-dialog.html',
})
export class AddUserDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<AddUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private roomService: RoomService,
    private router: Router) {}

  onNoClickAction(): void {
    this.dialogRef.close();
  }



  onAddUserAction(): void {
    const nameRoom = this.data.name;
    const nbOfDesks = this.data.numberOfDesks;
    const test2 = 'asd';
    const test3 = 'asd';
    const ctrl = this;

    const room: Room = {desks: [], name: nameRoom, numberOfDesks: nbOfDesks};
    this.roomService.addRoom(room)
      .subscribe(result => this.roomService.saveFile(result.id, this.data.fileToUpload)
        .subscribe(() => this.router.navigateByUrl('edit-room')));
    this.dialogRef.close();
  }

}
