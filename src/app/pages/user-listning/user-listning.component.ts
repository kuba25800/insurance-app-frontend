import {Component, Inject, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {UserService} from '../../services/user.service';
import {User} from '../../models/User';

export interface DialogData {
  id: number;
}

export interface TableElement {
  id: number;
  login: string;
  role: string;
}

let ELEMENT_DATA: TableElement[] = [];

@Component({
  selector: 'app-user-listning',
  templateUrl: './user-listning.component.html',
  styleUrls: ['./user-listning.component.css']
})
export class UserListningComponent implements OnInit {

  displayedColumns: string[] = ['id', 'login', 'role', 'action'];
  dataSource = new MatTableDataSource<TableElement>();

  constructor(private userService: UserService, private router: Router, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.userService.getUsers()
      .subscribe( users => this.prepareTableUsers(users) );
  }

  prepareTableUsers(users: User[]): void {
    ELEMENT_DATA = [];
    users.forEach(user => ELEMENT_DATA.push(this.prepareSingleUser(user)));

    this.dataSource.data = ELEMENT_DATA;
  }

  prepareSingleUser(user: User): TableElement {
    return {
      id: user.id,
      login: user.username,
      role: user.role
    };
  }

  goToCalculation(id: number): void {
    this.router.navigate(['calculation', id, 'data']);
  }

  deleteUser(id): void {
    const dialogRef = this.dialog.open(DeleteUserDialogComponent, {
      width: '400px',
      data: {id: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.userService.getUsers()
        .subscribe( user => this.prepareTableUsers(user) );
    });
  }

  editUser(id): void {
    this.router.navigate(['edit-user', id]);
  }

  showPolicyPossible(role: string): boolean {
    return 'user' === role;
  }

  showPolicy(username): void {
    this.router.navigate(['policies-listning', username]);
  }
}

@Component({
  selector: 'app-delete-user-dialog-component',
  templateUrl: 'delete-user-dialog.html',
})
export class DeleteUserDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DeleteUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private userService: UserService) {}

  onNoClickAction(): void {
    this.dialogRef.close();
  }

  onDeleteUserAction(): void {
    this.userService.delete(this.data.id).subscribe();
    this.dialogRef.close();
  }

}
