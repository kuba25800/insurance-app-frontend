import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {Room} from '../../models/Room';
import {NgModel} from '@angular/forms';
import {RoomService} from '../../services/room.service';

export interface TableElement {
  id: number;
  name: string;
  numberOfDesks: number;
}

let ELEMENT_DATA: TableElement[] = [];

@Component({
  selector: 'app-rooms-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.css']
})
export class RoomsListComponent implements OnInit {

  displayedColumns: string[] = ['name', 'numberOfDesks', 'action'];
  dataSource = new MatTableDataSource<TableElement>();
  model: Partial<Room> = {};
  showTable: boolean;
  choosenDate: string;

  constructor(private roomService: RoomService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.showTable = false;
  }

  prepareTableRooms(rooms: Room[]): void {
    this.showTable = true;
    ELEMENT_DATA = [];
    rooms.forEach(room => ELEMENT_DATA.push(this.prepareSingleRoom(room)));
    this.dataSource.data = ELEMENT_DATA;
  }

  prepareSingleRoom(room: Room): TableElement {
    return {
      id: room.id,
      name: room.name,
      numberOfDesks: room.numberOfDesks
    };
  }

  goToPolicy(lp: number): void {
    this.router.navigate(['policy-summary', lp]);
  }

  searchRoomByDate(name: NgModel): void {
    const date = name.viewModel;
    this.choosenDate = date;
    this.roomService.getRoomsByDate(date)
      .subscribe( rooms => this.prepareTableRooms(rooms) );
  }

  chooseRoom(id: number): void {
    const date = this.choosenDate;
    this.router.navigate(['room-reservation', id, date]);
  }
}
