import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {Room} from '../../models/Room';
import {RoomService} from '../../services/room.service';
import {PeriodService} from '../../services/period.service';
import {Period} from '../../models/Period';
import {User} from '../../models/User';
import {Desk} from '../../models/Desk';
import {UserDesk} from '../../models/UserDesk';

export interface TableElement {
  lp: number;
  user: string;
  name: string;
}

let ELEMENT_DATA: TableElement[] = [];

@Component({
  selector: 'app-room-reservation',
  templateUrl: './room-reservation.component.html',
  styleUrls: ['./room-reservation.component.css']
})
export class RoomReservationComponent implements OnInit {

  roomId: number;
  date: string;
  imageAddress: string;

  displayedColumns: string[] = ['lp', 'name', 'user', 'action'];
  dataSource = new MatTableDataSource<TableElement>();
  model: Partial<Room> = {};

  constructor(private roomService: RoomService, private route: ActivatedRoute, private periodService: PeriodService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.roomId = params.id;
      this.date = params.date;
      this.loadData();
      this.imageAddress = 'http://localhost:8080/api/room/image/download/' + this.roomId;
    });
  }

  loadData(): void {
    this.periodService.getPeriodsByDateAndRoom(this.date, this.roomId)
      .subscribe(periods => this.prepareTablePeriods(periods));
  }

  prepareTablePeriods(periods: UserDesk[]): void {
    ELEMENT_DATA = [];
    periods.forEach(period => ELEMENT_DATA.push(this.prepareSinglePeriod(period)));
    this.dataSource.data = ELEMENT_DATA;
  }

  goToRoom(id: number): void {
    const user = sessionStorage.getItem('username');
    this.periodService.getPeriodsByDateAndRoom(this.date, this.roomId)
      .subscribe(periods => this.prepareTablePeriods(periods));
  }

  prepareSinglePeriod(desk: UserDesk): TableElement {
    return {lp: desk.desk.lp, name: desk.desk.description, user: desk.username};
  }

  hasUser(user: User): boolean {
    return user !== null;
  }

  isSameUser(user: string): boolean {
    const sessionUser = sessionStorage.getItem('username');
    if (user === null) {
      return false;
    }
    return sessionUser === user;
  }

  reserve(lp: number): void {
    const user = sessionStorage.getItem('username');
    this.roomService.reserveRoom(this.date, this.roomId, user, lp)
      .subscribe(() => this.loadData());
  }
}
