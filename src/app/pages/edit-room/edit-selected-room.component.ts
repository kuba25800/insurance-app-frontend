import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Room} from '../../models/Room';
import {MatTableDataSource} from '@angular/material/table';
import {EditRoomService} from '../../services/edit-room.service';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-selected-room.component.html',
  styleUrls: ['./edit-selected-room.component.css']
})
export class EditSelectedRoomComponent implements OnInit {

  userId: number;
  dataSource = new MatTableDataSource<Room>();
  displayedColumns: string[] = ['name', 'numberOfDesks', 'action'];

  constructor(private route: ActivatedRoute, private editRoomService: EditRoomService) { }

  ngOnInit(): void {

  }

}
