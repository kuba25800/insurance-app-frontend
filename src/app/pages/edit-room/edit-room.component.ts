import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Room} from '../../models/Room';
import {MatTableDataSource} from '@angular/material/table';
import {RoomService} from '../../services/room.service';

export interface DialogData {
  id: number;
  username: string;
  password: string;
}

let ELEMENT_DATA: Room[] = [];

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-room.component.html',
  styleUrls: ['./edit-room.component.css']
})
export class EditRoomComponent implements OnInit {

  userId: number;
  dataSource = new MatTableDataSource<Room>();
  displayedColumns: string[] = ['name', 'numberOfDesks', 'action'];

  constructor(private route: ActivatedRoute, private roomService: RoomService, private router: Router) { }

  ngOnInit(): void {
    this.roomService.getRooms()
      .subscribe( rooms => this.prepareTablePolicies(rooms) );
  }

  prepareTablePolicies(rooms: Room[]): void {
    ELEMENT_DATA = [];
    rooms.forEach(room => ELEMENT_DATA.push(room));

    this.dataSource.data = ELEMENT_DATA;
  }

  chooseRoom(id: number): void {
    this.router.navigate(['edit-selected-room', id]);
  }

  deleteRoom(id: number): void {

  }

}
