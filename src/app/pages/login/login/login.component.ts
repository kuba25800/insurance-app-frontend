import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';
import { User } from '../../../models/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';
  invalidLogin = false;

  constructor(private router: Router,
              private loginservice: AuthenticationService) { }

  ngOnInit(): void {
  }

  checkLogin(): void {
    this.loginservice.authenticate(this.username, this.password)
      .subscribe(result => this.checkResult(result));
  }

  checkResult(authenticatedUser: User): void {
    this.invalidLogin = authenticatedUser == null;
  }

}
