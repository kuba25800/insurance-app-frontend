import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './pages/login/login/login.component';
import {AuthGuardService} from './services/auth-guard.service';
import {AuthAdminGuardService} from './services/auth-admin-guard.service';
import {UserListningComponent} from './pages/user-listning/user-listning.component';
import {AddRoomComponent} from './pages/add-user/add-room.component';
import {EditRoomComponent} from './pages/edit-room/edit-room.component';
import {EditSelectedRoomComponent} from './pages/edit-room/edit-selected-room.component';
import {RoomsListComponent} from './pages/rooms-list/rooms-list.component';
import {ReservationsListComponent} from './pages/reservations-list/reservations-list.component';
import {RoomReservationComponent} from './pages/room-reservation/room-reservation.component';

const routes: Routes = [
  { path: '', redirectTo: '/rooms-list', pathMatch: 'full' },
  { path: 'rooms-list', component: RoomsListComponent, canActivate: [AuthGuardService] },
  { path: 'reservations-list', component: ReservationsListComponent, canActivate: [AuthGuardService] },
  { path: 'room-reservation/:id/:date', component: RoomReservationComponent, canActivate: [AuthGuardService] },
  { path: 'user-listning', component: UserListningComponent, canActivate: [AuthAdminGuardService] },
  { path: 'add-room', component: AddRoomComponent, canActivate: [AuthAdminGuardService] },
  { path: 'edit-room', component: EditRoomComponent, canActivate: [AuthAdminGuardService] },
  { path: 'edit-selected-room/:id', component: EditSelectedRoomComponent, canActivate: [AuthAdminGuardService]},
  { path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
