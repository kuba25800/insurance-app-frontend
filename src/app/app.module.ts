import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatBadgeModule } from '@angular/material/badge';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { DatePipe } from '@angular/common';
import { LoginComponent } from './pages/login/login/login.component';
import { UserListningComponent } from './pages/user-listning/user-listning.component';
import { AddRoomComponent } from './pages/add-user/add-room.component';
import { EditRoomComponent } from './pages/edit-room/edit-room.component';
import { DevInterceptor } from './dev-interceptor.service';
import { EditSelectedRoomComponent } from './pages/edit-room/edit-selected-room.component';
import { RoomsListComponent } from './pages/rooms-list/rooms-list.component';
import { ReservationsListComponent } from './pages/reservations-list/reservations-list.component';
import { RoomReservationComponent } from './pages/room-reservation/room-reservation.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserListningComponent,
    AddRoomComponent,
    EditRoomComponent,
    EditSelectedRoomComponent,
    RoomsListComponent,
    ReservationsListComponent,
    RoomReservationComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatBadgeModule,
        MatFormFieldModule,
        MatStepperModule,
        MatTabsModule,
        MatInputModule,
        MatOptionModule,
        MatSelectModule,
        MatGridListModule,
        MatButtonModule,
        MatDialogModule,
        MatTableModule
    ],
  exports: [
        MatBadgeModule,
        MatFormFieldModule,
        MatTabsModule,
        MatInputModule,
        MatSelectModule,
        MatGridListModule,
        MatTableModule
    ],
  providers: [DatePipe,
    { provide: HTTP_INTERCEPTORS, useClass: DevInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
